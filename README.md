# Run Database for the BPC-BDS course

Clone the repository: 

and run the following command in the root directory.
```shell
$ docker-compose up
```

This project is solution so BPC-BDS assigment 1.

Database schema should represent simple tool for malware analysis that stores data about network traffic, events inside 
sandbox and submits these data to AV vendors.
