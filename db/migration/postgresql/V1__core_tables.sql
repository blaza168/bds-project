DROP TABLE IF EXISTS layer_header;
DROP TABLE IF EXISTS header_type;
DROP TABLE IF EXISTS procol;
DROP TABLE IF EXISTS packet;
DROP TABLE IF EXISTS submitter;
DROP TABLE IF EXISTS file;

-- File submitted into sandbox
CREATE TABLE file (
    id_file             BIGSERIAL PRIMARY KEY,
    filename            VARCHAR(80) NOT NULL,

    hash                VARCHAR(64) NOT NULL, -- SHA256 file hash
    packer              VARCHAR(30) DEFAULT NULL, -- packer used to pack the file
    architecture        VARCHAR(6) NOT NULL -- x86 | x86-64
);

-- Sandbox
CREATE TABLE sandbox (
    id_sandbox          BIGSERIAL PRIMARY KEY,
    id_file             BIGINT NOT NULL,
    spawn_time          TIMESTAMP NOT NULL, -- When sandbox has been spawned
    termination_time    TIMESTAMP DEFAULT NULL, -- When sandbox terminated

    FOREIGN KEY (id_file) REFERENCES file(id_file) ON DELETE CASCADE
);

-- Source of file
CREATE TABLE source (
    id_source           BIGSERIAL PRIMARY KEY,
    id_file             BIGINT NOT NULL,

    isolated_time       TIMESTAMP NOT NULL, -- when file was isolated
    hostname            VARCHAR(30) DEFAULT NULL, -- hostname of computer
    domain              VARCHAR(40) DEFAULT NULL, -- domain of computer
    executing_user      VARCHAR(50) DEFAULT NULL, -- name of user that process was running under

    FOREIGN KEY (id_file) REFERENCES file(id_file)
);