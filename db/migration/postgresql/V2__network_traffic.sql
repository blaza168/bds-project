-- Packets received/sent by processes inside sandbox
CREATE TABLE packet (
    id_packet       BIGSERIAL PRIMARY KEY,
    id_sandbox      BIGINT NOT NULL,

    generated_time  TIMESTAMP NOT NULL, -- When packet was sent/received
    source_process  VARCHAR(100) NOT NULL, -- Who sent/received this packet
    final_body      TEXT DEFAULT NULL, -- Body of L7

    FOREIGN KEY (id_sandbox) REFERENCES sandbox(id_sandbox)
);

-- Log only normal TCP/IP packets from L3 above
-- Other packets log into "operation" table. For example: ICMP packets
CREATE TABLE layer_header (
    id_layer_header BIGSERIAL PRIMARY KEY,
    id_packet       BIGINT NOT NULL,

    name            VARCHAR(20) NOT NULL, -- name of header
    value           VARCHAR(50) NOT NULL, -- value of header
    layer_number    SMALLINT NOT NULL, -- Layer number in TCP/IP.

    FOREIGN KEY (id_packet) REFERENCES packet(id_packet) ON DELETE CASCADE
);

