-- Type of event. For example: Application executed something.
CREATE TABLE event_type (
    id_event_type             SERIAL PRIMARY KEY,
    name                      VARCHAR(30) NOT NULL
);

-- Processes' behaviour. Some event that occurred.
CREATE TABLE event (
    id_event        BIGSERIAL PRIMARY KEY,
    id_sandbox      BIGINT NOT NULL,
    id_event_type   INT NOT NULL,

    generated_time  TIMESTAMP NOT NULL, -- WHen event was generated
    source_process  VARCHAR(100) NOT NULL, -- Source of this event

    FOREIGN KEY (id_sandbox) REFERENCES sandbox(id_sandbox) ON DELETE CASCADE,
    FOREIGN KEY (id_event_type) REFERENCES event_type (id_event_type) ON DELETE CASCADE
);

-- Names of event's data
-- Example: hash, source process, source hash, target hash, ...
CREATE TABLE event_description_type (
    id_event_description_type      SERIAL PRIMARY KEY,
    name                           VARCHAR(30) NOT NULL
);

-- Data associated with event.
-- Containing values for specific field characterising event
CREATE TABLE event_data (
    id_event_data                   BIGSERIAL PRIMARY KEY,
    id_event                        BIGINT NOT NULL,
    id_event_description_type       INT NOT NULL,

    event_value             VARCHAR(150) NOT NULL, -- Each threat event description can be converted into string.

    FOREIGN KEY (id_event) REFERENCES event(id_event) ON DELETE CASCADE,
    FOREIGN KEY (id_event_description_type) REFERENCES event_description_type(id_event_description_type) ON DELETE CASCADE
);

