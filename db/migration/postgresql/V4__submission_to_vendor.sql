-- Ioc type for manual insertion
-- Are we inserting hash ? Mutex ? IP address ? encoded IP address ?
CREATE TABLE manual_ioc_type (
    id_manual_ioc_type      SERIAL PRIMARY KEY,
    name                    VARCHAR(30) UNIQUE
);

-- Table for inserting ioc manually
-- Not all IOCs must be logged in other tables.
CREATE TABLE manual_ioc (
    id_manual_ioc           SERIAL PRIMARY KEY,
    id_manual_ioc_type      INT NOT NULL,
    value                   VARCHAR(250) NOT NULL, -- Most IOCs can be converted into string

    FOREIGN KEY (id_manual_ioc_type) REFERENCES manual_ioc_type(id_manual_ioc_type) ON DELETE CASCADE
);

-- AV vendor
-- This is entity that we submit our IOCs to
CREATE TABLE vendor (
    id_vendor               SERIAL PRIMARY KEY,
    name                    VARCHAR(50) -- name of AV vendor
);

-- MAP IOCs TO VENDOR (submissions)
-- Since IOC can be submitted from different tables, there must be relationship from vendor to each table.
-- Or create one "joining" table with 3 columns referencing all tables and make them nullable.
CREATE TABLE layer_header_vendor (
    id_vendor               INT NOT NULL,
    id_layer_header         INT NOT NULL,

    FOREIGN KEY (id_layer_header) REFERENCES layer_header(id_layer_header) ON DELETE CASCADE,
    FOREIGN KEY (id_vendor) REFERENCES vendor(id_vendor) ON DELETE CASCADE
);

CREATE TABLE event_data_vendor (
    id_vendor               INT NOT NULL,
    id_event_data           INT NOT NULL,

    FOREIGN KEY (id_vendor) REFERENCES vendor(id_vendor) ON DELETE CASCADE,
    FOREIGN KEY (id_event_data) REFERENCES event_data(id_event_data) ON DELETE CASCADE
);

CREATE TABLE manual_ioc_vendor (
    id_vendor               INT NOT NULL,
    id_manual_ioc           INT NOT NULL,

    FOREIGN KEY (id_manual_ioc) REFERENCES manual_ioc(id_manual_ioc) ON DELETE CASCADE,
    FOREIGN KEY (id_vendor) REFERENCES vendor(id_vendor) ON DELETE CASCADE
);
