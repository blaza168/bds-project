CREATE TABLE user_account (
    id_user         BIGSERIAL PRIMARY KEY,
    username        VARCHAR(30) NOT NULL,
    password        VARCHAR(60) NOT NULL
);

-- Password: solarwinds123
INSERT INTO user_account (username, password) VALUES ('intern', '$2a$10$WecymzzLP8.DJF1K2bbyCejhhBjjJoQ92Ih/czZagImnCK.aiIapK');
