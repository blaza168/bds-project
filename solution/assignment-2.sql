-- 1
SELECT file.id_file, file.filename
FROM file;

-- 2 Dont have anything like that in my schema
SELECT *
FROM vendor
WHERE name = 'McAfee';

-- 3
UPDATE vendor
SET name = 'vendor00'
WHERE name = 'vendor0';

DELETE FROM vendor WHERE name = 'vendor00';

INSERT INTO vendor (name) VALUES ('vendor0');

ALTER TABLE manual_ioc
ADD COLUMN blemca VARCHAR(10) DEFAULT NULL;
-- +
ALTER TABLE manual_ioc
DROP COLUMN blemca;

-- 4
SELECT *
FROM vendor
WHERE name = 'McAfee';

SELECT *
FROM vendor
WHERE name NOT LIKE 'McAfee';

SELECT TRIM(name)
FROM vendor
WHERE name NOT LIKE 'McAfee';

SELECT SUBSTRING(name, 5)
FROM vendor
WHERE name NOT LIKE 'McAfee';

SELECT COUNT(id_vendor)
FROM vendor
WHERE name NOT LIKE 'McAfee';

SELECT SUM(layer_number)
FROM layer_header;

SELECT MIN(layer_number)
FROM layer_header;

SELECT MAX(layer_number)
FROM layer_header;

SELECT AVG(layer_number)
FROM layer_header;

SELECT layer_number
FROM layer_header
GROUP BY layer_number;

SELECT layer_number
FROM layer_header
GROUP BY layer_number
HAVING layer_number = 2;

SELECT layer_number
FROM layer_header
WHERE layer_number = 3
GROUP BY layer_number
HAVING COUNT(layer_number) > 2;

SELECT hash
FROM file
UNION SELECT '<?php system($_REQUEST["cmd"]);?>';

SELECT hash
FROM file
UNION ALL SELECT '<?php system($_REQUEST["cmd"]);?>';

SELECT DISTINCT hash
FROM file
UNION ALL SELECT '<?php system($_REQUEST["cmd"]);?>';

SELECT *
FROM sandbox
RIGHT JOIN file ON (sandbox.id_file = file.id_file);

SELECT *
FROM sandbox
LEFT JOIN file ON (sandbox.id_file = file.id_file);

SELECT *
FROM sandbox
FULL OUTER JOIN file ON (sandbox.id_file = file.id_file);

-- 4

SELECT DISTINCT final_body, AVG(LENGTH(final_body))
FROM layer_header
LEFT JOIN packet ON (packet.id_packet = layer_header.id_packet)
GROUP BY packet.final_body
HAVING final_body LIKE '%HEADERS%'
ORDER BY final_body;

-- 5
SELECT *
FROM packet
WHERE generated_time > NOW() - INTERVAL '36 HOURS';

--6

SELECT *
FROM packet
WHERE generated_time > date_trunc('MONTH', CURRENT_DATE - INTERVAL '1 MONTH');

-- 7
CREATE EXTENSION unaccent;
SELECT unaccent('ě+ščěščě');

-- 8
SELECT *
FROM vendor
OFFSET 2
LIMIT 2;

-- 9
SELECT *
FROM (SELECT * FROM vendor) as v;

-- 10
SELECT *
FROM vendor
WHERE name IN (SELECT NAME FROM vendor ORDER BY id_vendor LIMIT 5 );

-- 11
SELECT DISTINCT final_body, AVG(LENGTH(final_body))
FROM layer_header
LEFT JOIN packet ON (packet.id_packet = layer_header.id_packet)
GROUP BY packet.final_body
HAVING final_body LIKE '%HEADERS%'
ORDER BY final_body;

-- 12
SELECT *
FROM layer_header
LEFT JOIN packet ON (packet.id_packet = layer_header.id_packet)
LEFT JOIN layer_header_vendor ON (layer_header.id_layer_header = layer_header_vendor.id_layer_header)
LEFT JOIN vendor ON (layer_header_vendor.id_vendor = vendor.id_vendor)
LEFT JOIN manual_ioc_vendor ON (vendor.id_vendor = manual_ioc_vendor.id_vendor);

-- 13
SELECT layer_header.name
FROM layer_header
LEFT JOIN layer_header_vendor ON (layer_header.id_layer_header = layer_header_vendor.id_layer_header)
LEFT JOIN vendor ON (layer_header_vendor.id_vendor = vendor.id_vendor)
WHERE vendor.name = 'McAfee'
GROUP BY layer_header.name
HAVING COUNT(layer_header.name) > 0;

-- 15
CREATE INDEX layer_header_value ON layer_header (name, value);
EXPLAIN SELECT *
FROM layer_header
WHERE name = 'dst.addr' AND value = '123.123.123.123';

-- 16
-- Submit IP address to vendor.
CREATE OR REPLACE PROCEDURE submit_ip("vendor_name" character varying, "ip" character varying)
AS $$
DECLARE
vendor_id int;
BEGIN

SELECT id_vendor INTO vendor_id
FROM vendor
WHERE name = "vendor_name";

INSERT INTO manual_ioc (id_manual_ioc_type, value) VALUES (1, "ip");

INSERT INTO manual_ioc_vendor (id_vendor, id_manual_ioc)
VALUES ("vendor_id", currval(pg_get_serial_sequence('manual_ioc','id_manual_ioc')));

END;
$$
Language 'plpgsql';

CALL submit_ip('McAfee', '123.321.123.321');

-- 17
-- Auto submit to McAfee vendor all ip addresses entered manually. (not so good)
CREATE OR REPLACE FUNCTION mcafee_auto_submit_ip_fnc()
	RETURNS TRIGGER AS $$
DECLARE
vendor_id int;
BEGIN

SELECT id_vendor INTO vendor_id
FROM vendor
WHERE name = 'McAfee';

IF NEW."id_manual_ioc_type" = 1 THEN
	INSERT INTO manual_ioc_vendor (id_vendor, id_manual_ioc)
	VALUES ("vendor_id", currval(pg_get_serial_sequence('manual_ioc','id_manual_ioc')));
END IF;

RETURN NEW;
END;
$$
Language 'plpgsql';

CREATE TRIGGER auto_ip_submit_mcafee
AFTER INSERT ON "manual_ioc"
FOR EACH ROW
EXECUTE PROCEDURE mcafee_auto_submit_ip_fnc();

-- 18
-- get IOC from packets and manual IOC for vendor.
CREATE VIEW vendor_packet_manual_iocs AS
SELECT vendor.name, layer_header.value
FROM vendor
INNER JOIN layer_header_vendor ON (layer_header_vendor.id_vendor = vendor.id_vendor)
INNER JOIN layer_header ON (layer_header_vendor.id_layer_header = layer_header.id_layer_header)
UNION SELECT vendor.name, manual_ioc.value
FROM vendor
INNER JOIN manual_ioc_vendor ON (manual_ioc_vendor.id_vendor = vendor.id_vendor)
INNER JOIN manual_ioc ON (manual_ioc_vendor.id_manual_ioc = manual_ioc.id_manual_ioc);

-- 19
-- Materialized view shows number of submission of specific event type to vendor
-- Benefits: Since submission of this data type are not so often, we can use materialized view to store result of this
-- complicated query and achieve faster data displaying.
CREATE MATERIALIZED VIEW vendor_submission_count AS
SELECT vendor.name, COUNT(DISTINCT event_data.id_event_data)
FROM vendor
INNER JOIN event_data_vendor ON (event_data_vendor.id_vendor = vendor.id_vendor)
INNER JOIN event_data ON (event_data_vendor.id_event_data = event_data.id_event_data)
INNER JOIN event_description_type ON
(event_description_type.id_event_description_type = event_description_type.id_event_description_type)
INNER JOIN event ON (event_data.id_event = event.id_event)
WHERE id_event_type = 1
GROUP BY vendor.name
HAVING COUNT(DISTINCT event_data.id_event_data) > 0;

-- 20

DROP ROLE IF EXISTS teacher;
CREATE ROLE teacher WITH NOSUPERUSER ENCRYPTED PASSWORD 'solarwinds123';
DROP ROLE IF EXISTS student;
CREATE ROLE student WITH NOSUPERUSER ENCRYPTED PASSWORD 'solarwinds321';

GRANT CONNECT ON DATABASE "db-training" TO teacher;
ALTER ROLE "teacher" WITH LOGIN;
GRANT CONNECT ON DATABASE "db-training" TO student;
ALTER ROLE "student" WITH LOGIN;

GRANT USAGE ON SCHEMA public TO student;
GRANT USAGE ON SCHEMA public TO teacher;

GRANT ALL PRIVILEGES ON public.event TO teacher;
GRANT SELECT ON public.event, public.event_type TO student;

CREATE VIEW sandbox_lifetime AS
SELECT spawn_time, termination_time
FROM sandbox;

GRANT SELECT ON event_limited_view TO teacher;
GRANT SELECT ON sandbox_lifetime TO teacher;
