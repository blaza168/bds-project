
CREATE TABLE file (
    id_file             INT auto_increment PRIMARY KEY,
    filename            VARCHAR(80) NOT NULL,

    hash                VARCHAR(64) NOT NULL, -- SHA256 file hash
    packer              VARCHAR(30) DEFAULT NULL,
    architecture        VARCHAR(6) NOT NULL -- x86 | x86-64
);

CREATE TABLE sandbox (
    id_sandbox          INT auto_increment PRIMARY KEY,
    id_file             INT NOT NULL,
    spawn_time          DATETIME NOT NULL,
    termination_time    DATETIME DEFAULT NULL, -- SQLINES DEMO *** inated

    FOREIGN KEY (id_file) REFERENCES file(id_file) ON DELETE CASCADE
);

CREATE TABLE source (
    id_source           INT auto_increment PRIMARY KEY,
    id_file             INT NOT NULL,

    isolated_time       DATETIME NOT NULL,
    hostname            VARCHAR(30) DEFAULT NULL,
    domain              VARCHAR(40) DEFAULT NULL, -- domain of computer
    executing_user      VARCHAR(50) DEFAULT NULL,

    FOREIGN KEY (id_file) REFERENCES file(id_file)
);

CREATE TABLE packet (
    id_packet       INT auto_increment PRIMARY KEY,
    id_sandbox      INT NOT NULL,

    generated_time  DATETIME NOT NULL,
    source_process  VARCHAR(100) NOT NULL,
    final_body      TEXT DEFAULT NULL,

    FOREIGN KEY (id_sandbox) REFERENCES sandbox(id_sandbox)
);

CREATE TABLE layer_header (
    id_layer_header INT auto_increment PRIMARY KEY,
    id_packet       INT NOT NULL,

    name            VARCHAR(20) NOT NULL, -- name of header
    value           VARCHAR(50) NOT NULL, -- value of header
    layer_number    INT(1) NOT NULL,

    FOREIGN KEY (id_packet) REFERENCES packet(id_packet) ON DELETE CASCADE
);

CREATE TABLE event_type (
    id_event_type             INT auto_increment PRIMARY KEY,
    name                      VARCHAR(30) NOT NULL
);

CREATE TABLE event (
    id_event        INT auto_increment PRIMARY KEY,
    id_sandbox      INT NOT NULL,
    id_event_type   INT NOT NULL,

    generated_time  DATETIME NOT NULL,
    source_process  VARCHAR(100) NOT NULL,

    FOREIGN KEY (id_sandbox) REFERENCES sandbox(id_sandbox) ON DELETE CASCADE,
    FOREIGN KEY (id_event_type) REFERENCES event_type (id_event_type) ON DELETE CASCADE
);

CREATE TABLE event_description_type (
    id_event_description_type      INT auto_increment PRIMARY KEY,
    name                           VARCHAR(30) NOT NULL
);

CREATE TABLE event_data (
    id_event_data                   INT auto_increment PRIMARY KEY,
    id_event                        INT NOT NULL,
    id_event_description_type       INT NOT NULL,

    event_value             VARCHAR(150) NOT NULL,

    FOREIGN KEY (id_event) REFERENCES event(id_event) ON DELETE CASCADE,
    FOREIGN KEY (id_event_description_type) REFERENCES event_description_type(id_event_description_type) ON DELETE CASCADE
);

CREATE TABLE ioc_type (
    id_ioc_type             INT auto_increment PRIMARY KEY,
    name                    VARCHAR(50)
);

CREATE TABLE submitted_ioc (
    id_submitted_ioc        INT auto_increment PRIMARY KEY,
    id_sandbox              INT NOT NULL,
    id_ioc_type             INT NOT NULL,

    value                   VARCHAR(250) NOT NULL,

    FOREIGN KEY (id_ioc_type) REFERENCES ioc_type(id_ioc_type) ON DELETE CASCADE,
    FOREIGN KEY (id_sandbox) REFERENCES sandbox(id_sandbox) ON DELETE CASCADE
);

CREATE TABLE vendor (
    id_vendor               INT auto_increment PRIMARY KEY,
    name                    VARCHAR(50)
);

CREATE TABLE vendor_ioc (
    id_vendor               INT NOT NULL,
    id_submitted_ioc        INT NOT NULL,

    FOREIGN KEY (id_vendor) REFERENCES vendor(id_vendor) ON DELETE CASCADE,
    FOREIGN KEY (id_submitted_ioc) REFERENCES submitted_ioc(id_submitted_ioc) ON DELETE CASCADE
);